package com.miaoshaproject.dao;

import com.miaoshaproject.dataobject.UserPassWordDo;

public interface UserPassWordDoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserPassWordDo record);

    int insertSelective(UserPassWordDo record);

    UserPassWordDo selectByPrimaryKey(Integer id);
    UserPassWordDo selectByUserId(Integer id);


    int updateByPrimaryKeySelective(UserPassWordDo record);

    int updateByPrimaryKey(UserPassWordDo record);
}