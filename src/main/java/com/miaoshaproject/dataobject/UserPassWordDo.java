package com.miaoshaproject.dataobject;

public class UserPassWordDo {
    private Integer id;

    private String encrptPasswrod;

    private Integer userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEncrptPasswrod() {
        return encrptPasswrod;
    }

    public void setEncrptPasswrod(String encrptPasswrod) {
        this.encrptPasswrod = encrptPasswrod == null ? null : encrptPasswrod.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}