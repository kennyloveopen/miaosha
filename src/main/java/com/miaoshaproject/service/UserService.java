package com.miaoshaproject.service;

import com.miaoshaproject.error.BussinessException;
import com.miaoshaproject.service.model.UserModel;

/**
 * Created by kenny on 2019/1/5.
 */
public interface UserService {


      UserModel getUserById(Integer id);
      void register (UserModel userModel) throws BussinessException;

      UserModel validateLogin(String telphone, String password) throws BussinessException;
}
