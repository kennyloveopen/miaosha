package com.miaoshaproject.service.impl;

import com.miaoshaproject.dao.UserDOMapper;
import com.miaoshaproject.dao.UserPassWordDoMapper;
import com.miaoshaproject.dataobject.UserDO;
import com.miaoshaproject.dataobject.UserPassWordDo;
import com.miaoshaproject.error.BussinessException;
import com.miaoshaproject.error.EmBussinessError;
import com.miaoshaproject.service.UserService;
import com.miaoshaproject.service.model.UserModel;
import com.miaoshaproject.validator.ValidationImpl;
import com.miaoshaproject.validator.ValidationResult;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.beans.Transient;

/**
 * Created by kenny on 2019/1/5.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDOMapper userDOMapper;
    @Autowired
    private UserPassWordDoMapper userPassWordDoMapper;
    @Autowired
    private ValidationImpl validation;
    @Override
    public UserModel getUserById(Integer id) {
        UserDO userDO = userDOMapper.selectByPrimaryKey(id);
        UserPassWordDo userPassWordDo = userPassWordDoMapper.selectByUserId(id);

        return convertFromDataObject(userDO,userPassWordDo);


    }

    public UserModel validateLogin(String telphone, String passwrod) throws  BussinessException{
        UserDO userDO = userDOMapper.selectByTelphone(telphone);
        if(userDO ==null) {
            throw new BussinessException(EmBussinessError.USEER_LOGIN_FAIL);
        }
        UserPassWordDo userPassWordDo = userPassWordDoMapper.selectByUserId(userDO.getId());
        if(!StringUtils.equals(passwrod,userPassWordDo.getEncrptPasswrod())){
            throw new BussinessException(EmBussinessError.USEER_LOGIN_FAIL);
        }
        UserModel userModel = convertFromDataObject(userDO,userPassWordDo);

        return userModel;
    }

    @Override
    @Transactional
    public void register(UserModel userModel) throws BussinessException {
        if(userModel==null){
            throw  new BussinessException(EmBussinessError.PARAMETER_VALIDATION_ERROR);
        }
       ValidationResult validationResult= validation.validate(userModel);
        if(validationResult.isHasErrors()){
            throw  new BussinessException(EmBussinessError.PARAMETER_VALIDATION_ERROR, validationResult.getErrMsg());
        }
        UserDO userDO = convertFromModel(userModel);
        try{
            userDOMapper.insertSelective(userDO);
        }catch (DuplicateKeyException e){
            e.printStackTrace();
            throw new BussinessException(EmBussinessError.PARAMETER_VALIDATION_ERROR,"手机好重复");
        }

        userModel.setId(userDO.getId());
        UserPassWordDo userPassWordDo =converPasswordFromModel(userModel);
        userPassWordDoMapper.insertSelective(userPassWordDo);
    }

    private UserPassWordDo converPasswordFromModel(UserModel userModel){
        UserPassWordDo userPassWordDo =new UserPassWordDo();
        userPassWordDo.setEncrptPasswrod(userModel.getEncrptPassword());
        userPassWordDo.setUserId(userModel.getId());
        return userPassWordDo;
    }
    public  UserDO convertFromModel(UserModel userModel){

        UserDO userDO = new UserDO();
        BeanUtils.copyProperties(userModel,userDO);
        return userDO;
    }

    public UserModel convertFromDataObject(UserDO userDo, UserPassWordDo userPassWordDo){
        if(userDo==null){
            return null;
        }
        UserModel userModel = new UserModel();
        BeanUtils.copyProperties(userDo,userModel);
        if(userPassWordDo!=null){
            userModel.setEncrptPassword(userPassWordDo.getEncrptPasswrod());
        }
        return userModel;
    }
}
