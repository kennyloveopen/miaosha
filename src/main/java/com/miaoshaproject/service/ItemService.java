package com.miaoshaproject.service;

import com.miaoshaproject.error.BussinessException;
import com.miaoshaproject.service.model.ItemModel;

import java.util.List;

/**
 * Created by kenny on 2019/1/6.
 */
public interface  ItemService {


    //创建商品

    ItemModel createItem(ItemModel itemModel) throws BussinessException;

    //商品列表浏览
    List<ItemModel> listIitem();

    //商品详情浏览
    ItemModel getItemById(Integer id);
}
