package com.miaoshaproject.error;

/**
 * Created by kenny on 2019/1/5.
 */
public enum  EmBussinessError implements CommonError {



    PARAMETER_VALIDATION_ERROR(10001,"参数不合法"),
    UNKNOWN_ERROR(10002,"未知错误"),


    USEER_NOT_EXIST(20001,"用户不存在"),

    USEER_LOGIN_FAIL(20002,"用户手机号或密码不正确");
    private int errCode;
    private String errMsg;


    private EmBussinessError(int errCode, String errMsg) {
        this.errCode=errCode;
        this.errMsg =errMsg;
    }


    @Override
    public int geteErrCode() {
        return this.errCode;
    }

    @Override
    public String getErrMsg() {
        return this.errMsg;
    }

    @Override
    public CommonError setErrMsg(String errMsg) {
         this.errMsg = errMsg;
         return  this;
    }
}
