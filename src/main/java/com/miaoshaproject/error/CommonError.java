package com.miaoshaproject.error;

/**
 * Created by kenny on 2019/1/5.
 */
public interface CommonError {

    public int geteErrCode();
    public String getErrMsg();
    public CommonError setErrMsg(String errMsg);
}
