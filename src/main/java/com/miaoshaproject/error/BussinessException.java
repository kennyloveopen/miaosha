package com.miaoshaproject.error;

/**
 * Created by kenny on 2019/1/5.
 */
public class BussinessException extends Exception implements CommonError {


     private CommonError commonError;

     public BussinessException(CommonError commonError){
         super();
         this.commonError = commonError;
     }

     //接受自定义Errormsg
    public BussinessException(CommonError commonError, String errMsg){
        super();
        this.commonError = commonError;
        this.commonError.setErrMsg(errMsg);

    }

    @Override
    public int geteErrCode() {
        return this.commonError.geteErrCode();
    }

    @Override
    public String getErrMsg() {
        return this.commonError.getErrMsg();
    }

    @Override
    public CommonError setErrMsg(String errMsg) {
        this.commonError.setErrMsg(errMsg);
        return this;
    }
}
